/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package contabanco;

/**
 *
 * @author henri
 */
public class NovaConta {
    
    public int numConta; // 0000-00
    protected String tipoConta; // CC CP
    private String usuarioConta;
    private float saldoConta;
    private boolean statusConta;
    
    // Construtor
    public NovaConta() {
        this.saldoConta = 0;
        this.statusConta = false;
    }
    // Getter and Setter
    public int getNumConta() {
        return this.numConta;
    }

    public void setNumConta(int numConta) {
        this.numConta = numConta;
    }

    public String getTipoConta() {
        return this.tipoConta;
    }

    public void setTipoConta(String tipoConta) {
        this.tipoConta = tipoConta;
    }

    public String getUsuarioConta() {
        return this.usuarioConta;
    }

    public void setUsuarioConta(String usuarioConta) {
        this.usuarioConta = usuarioConta;
    }

    public float getSaldoConta() {
        return this.saldoConta;
    }

    private void setSaldoConta(float saldoConta) {
        this.saldoConta = saldoConta;
    }

    public boolean isStatusConta() {
        return this.statusConta;
    }

    private void setStatusConta(boolean statusConta) {
        this.statusConta = statusConta;
    }
    
    public void getResume(){
        System.out.println("Usuário: " + getUsuarioConta());
        System.out.println("Numero da Conta : " + getNumConta());
        System.out.println("Tipo de conta: " + getTipoConta());
        System.out.println("Conta ativa: " + isStatusConta());
        System.out.println("Saldo atual: " + getSaldoConta());
        
    }
        // Abrir conta
    public void abriConta(String tipoC){
        setTipoConta(tipoC);
        setStatusConta(true);
        if (tipoC.equals("CC")) {
            setSaldoConta(50);
        }else if (tipoC.equals("CP")) {
            setSaldoConta(150);
        }
    }
    // Fechar conta
    public void fecharConta(){
        if (getSaldoConta() > 0) {
            System.out.println("Saldo Existente. Impossivel fechar conta.");
        }else if (getSaldoConta() < 0) {
            System.out.println("Saldo devedor! Impossivel fechar conta.");
        }else{
            setStatusConta(false);
        }
    }
    // Depositar
    public void depositar(float valor){
        if (isStatusConta() == true) {
            setSaldoConta(getSaldoConta() + valor);
        }else{
            System.out.println("Conta Innativa ou inexistente");
        }
    }
    // Sacar
    public void sacar(float valor){
        if (isStatusConta() == true) {
            if (getSaldoConta() > 0) {
                setSaldoConta(getSaldoConta() - valor);
            }else if (getSaldoConta() <= 0) {
                System.out.println("Saldo Insuficiente");
            }
        }else{
            System.out.println("Conta inativa ou inxistente");
        }
        
    }
    // Mensalidade
    public void mensalidade(){
        if (isStatusConta() == true) {
            if (getTipoConta().equals("CC")) {
                setSaldoConta(getSaldoConta() - 12);
            }else if (getTipoConta().equals("CP")) {
                setSaldoConta(getSaldoConta() - 20);
            }
        }else{
            System.out.println("Conta inativa ou inexistente");
        }
        
    }
}
