/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package contabanco;

/**
 *
 * @author henri
 */
public class ContaBanco {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        NovaConta conta1 = new NovaConta();
        
        conta1.abriConta("CC");
        conta1.setNumConta(1111);
        conta1.setUsuarioConta("joão da silva");
        
        conta1.getResume();
        System.out.println("");
        
        conta1.depositar(500);
        
        conta1.getResume();
        System.out.println("");
       
        conta1.sacar(650);
        conta1.getResume();
        
       
        
       
    }
    
}
