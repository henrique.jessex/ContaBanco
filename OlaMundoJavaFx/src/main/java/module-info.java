module com.cursoemvideo.olamundojavafx {
    requires javafx.controls;
    requires javafx.fxml;

    opens com.cursoemvideo.olamundojavafx to javafx.fxml;
    exports com.cursoemvideo.olamundojavafx;
}
