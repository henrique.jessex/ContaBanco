/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pooaula02;

/**
 *
 * @author henri
 */
public class Caneta {
    // Atributos
    private String modelo;
    private String cor;
    private float ponta;
    private int carga;
    private boolean tampada;
    // Construtor
    public Caneta(String mod, String cor, float ponta, int carg, boolean tamp){
        this.modelo = mod;
        this.cor = cor;
        this.ponta = ponta;
        this.carga = carg;
        this.tampada = tamp;
        /*
        this.modelo = "Bic";
        this.cor = "Azul";
        this.ponta = 0.5f;
        this.carga = 100;
        this.tampada = true;
        */
    }
    //Metodos GETTER 
    public String getModelo(){
        return this.modelo;
    }
    public String getCor(){
        return this.cor;
    }
    public float getPonta(){
        return this.ponta;
    }
    public int getCarga(){
        return this.carga;
    }
    public boolean getTampada(){
        return this.tampada;
    }
    // Metodos SETTER
    public void setModelo(String m){
        this.modelo = m;
    }
    public void setCor(String c){
        this.cor = c;
    }
    public void setPonta(float p){
        this.ponta = p;
    }
    public void setCarga(int c){
        this.carga = c;
    }
    public void setTampa(boolean t){
        this.tampada = t;
    }
    // verifica Status
    public void status(){
        System.out.println("Modelo: " + this.modelo);
        System.out.println("Cor: " + this.cor);
        System.out.println("Ponta: " + this.ponta);
        System.out.println("Carga: " + this.carga);
        System.out.println("tampada: " + this.tampada);
    }
    // Rabisca
    public void rabiscar(){
        if (this.tampada == true) {
            System.out.println("Erro! Não posso rabiscar");
        }else{
            System.out.println("Rabiscando");
        }
    }
}
