/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package pooaula02;

/**
 *
 * @author henri
 */
public class PooAula02 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Caneta c1 = new Caneta("zig", "azul", 0.5f, 50, true);
        Caneta c2 = new Caneta("trug", "black", 1.5f, 65, false);
        Canetinha c3 = new Canetinha("Zigug", "Verde", 2f, 78, false);
        
        
        
        System.out.println("Iniciando os objetos caneta:");
        c1.status();
        c1.rabiscar();
        c2.status();
        c2.rabiscar();
        c3.status();
        System.out.println("Fim Status.");
        
        c1.setCarga(100);
        c1.setCor("Azul");
        c1.setModelo("Bic tampa azul");
        c1.setPonta(.5f);
        
        c2.setCarga(50);
        c2.setCor("Amarela");
        c2.setModelo("MonBlanc");
        c2.setPonta(1.5f);
        
        System.out.println("Caneta 01");
       
        c1.setTampa(true);
        System.out.println("cor: " + c1.getCor());
        System.out.println("Modelo: " + c1.getModelo());
        System.out.println("Ponta: " + c1.getPonta());
        System.out.println("Carga: " + c1.getCarga());
        System.out.println("Esta tampada: " + c1.getTampada());
        c1.rabiscar();
       
        System.out.println("caneta 02");
        c2.setTampa(false);
        System.out.println("cor: " + c2.getCor());
        System.out.println("Modelo: " + c2.getModelo());
        System.out.println("Ponta: " + c2.getPonta());
        System.out.println("Carga: " + c2.getCarga());
        System.out.println("Esta tampado: " + c2.getTampada());
        c2.rabiscar();   
    }
}
