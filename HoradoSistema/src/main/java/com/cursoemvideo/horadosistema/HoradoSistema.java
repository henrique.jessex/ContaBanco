/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.cursoemvideo.horadosistema;

import java.util.Date;
import java.util.Locale;

/**
 *
 * @author henri
 */
public class HoradoSistema {

    public static void main(String[] args) {
        Date relogio = new Date();
        Locale locale = Locale.getDefault();
        String country = System.getProperty("user.country");
        String language = System.getProperty("user.language");
         
        
        System.out.println("User country: " + country);
        System.out.println("User language: " + language);
        System.out.print("LocaleDefault: ");
        System.out.println(locale.toString());
        System.out.print("A hora do sistema é: ");
        System.out.println(relogio.toString());
    }
}
