module com.descompila.primeirojavafx {
    requires javafx.controls;
    requires javafx.fxml;

    opens com.descompila.primeirojavafx to javafx.fxml;
    exports com.descompila.primeirojavafx;
}
